# uContract

uContract is a Design By Contract (DBC) tool implemented in Java. It is designed for writing contracts for event sourced aggregate roots in Domain-Driven Design (DDD). For implementing event sourced aggregate roots, see `EsAggregateRoot` in [ezddd-core](https://gitlab.com/TeddyChen/ezddd/-/tree/master/ezddd-core).

The preconditions are written before applying domain event, and the postconditions are written after applying domain event.
The aggregate invariants are checked in the `apply` method of event sourced aggregate, as [EsAggregateRoot](https://gitlab.com/TeddyChen/ezddd/-/blob/master/ezddd-core/src/main/java/tw/teddysoft/ezddd/entity/EsAggregateRoot.java). Note that uContract does not support subcontracting since we do not recommand using subclassing in event sourced aggregates.

## How to use

### Requirement

- JDK 17+

### Maven

```xml
<dependency>
    <groupId>tw.teddysoft.ucontract</groupId>
    <artifactId>uContract</artifactId>
    <version>LATEST</version>
</dependency>
```

### Documentation

- [Javadoc](https://www.javadoc.io/doc/tw.teddysoft.ucontract/uContract)

### Usage

#### Precondition

Preconditions are assertions that are checked before the execution of a method body. uContracts supports the following methods to specify preconditions:

- require(String annotation, BooleanSupplier assertion)
- requireNotNull(String annotation, Object obj)
- requireNotEmpty(String annotation, String str)

The first argument is the annotation of the precondition, which serves as an API document for the specified method. Additionally, if a precondition is violated, a PreconditionViolationException is thrown, and the annotation is attached to the exception for debugging purposes.

The second argument of the `require` method is a Boolean lambda expression used to define a precondition. In the case of `requireNotNull`, the second argument is an object, and for `requireNotEmpty`, it is a string.

#### Postcondition

Postconditions are assertions that are checked after the execution of a method body. uContracts supports three types of methods to specify postconditions. If a postcondition is violated, a PostconditionViolationException is thrown, and a message is attached to the exception for debugging purposes.

Type 1:

- ensure(String annotation, BooleanSupplier assertion)
- ensureNotNull(String annotation, Object obj)

The first argument is the annotation of the postcondition, which serves as an API document for the specified method. The second argument of the `ensure` method is a Boolean lambda expression used to define a postcondition. In the case of `ensureNotNull`, the second argument is an object.

Type 2:

- \<T> T ensureResult(String annotation, T result, Function<T, Boolean> assertion)
- \<T> T ensureImmutableCollection(T resultingCollection)

They are used in a query method to verify the result before returning it. It serves as a syntactic sugar for combining the actions of ensuring a result and returning the result in one step. In the case of `ensureResult`, the third parameter is a Boolean lambda expression used to verify the second parameter, `result`. In the case of `ensureImmutableCollection`, the parameter is the result to be checked.

Type 3:

- \<T> void ensureAssignable(T state, T oldState, String ... assignables)

It is used to check that fields of an object are not modified by a method body if the fields are not assignable. It compares all fields between the new state and old state, excluding those specified as assignable. The third parameter is a string array of assignable fields. Each element of the string array is a regular expression that represents an assignable field.

#### Class Invariant

Class invariant are assertions that are checked before and after the execution of a method body. uContracts supports the following methods to specify class invariant:

- invariant(String annotation, BooleanSupplier assertion)
- invariantNotNull(String annotation, Object obj)

The first argument is the annotation of the class invariant, which serves as an API document for the specified method. Additionally, if a class invariant is violated, a ClassInvariantViolationException is thrown, and the annotation is attached to the exception for debugging purposes.

The second argument of the `invariant` method is a Boolean lambda expression used to define a class invariant. In the case of `invariantNotNull`, the second argument is an object.

#### Help Methods

- boolean reject(String annotation, BooleanSupplier assertion)

The reject method is used in Domain-Driven Design (DDD) as a helper method to prevent the generation of unnecessary domain events under specific conditions. For example, when renaming a User aggregate, a UserRenamedEvent is typically generated. However, if the new name is the same as the old name, generating a UserRenamedEvent becomes unnecessary because there is no change in the User aggregate's state. The reject method can check for such conditions and halt the method execution, ensuring that the unnecessary event is not generated. Using reject explicitly communicates the intent to terminate the execution of the method body.

Reject is placed after the precondition and before the method body. Its first argument is a String to describe the reason of rejection, and the second argument is a Boolean lambda expression used to define the rejection condition. Since reject is design for DDD rather than DBC, disabling DBC will not affect its execution. Note that since the method body is not executed, postconditions and aggregate invariants are not checked if the method is rejected.

The following code snippet shows an example of how to use the reject method.

```java
public void changeNickname(String newNickname) {
    requireNotNull("Nickname", newNickname);
    require("Nickname is not empty", () -> !newNickname.isEmpty());
    if (reject("nick name equals", () -> getNickname().equals(newNickname))) return;

    apply(new UserEvents.NicknameChanged(this.userId, newNickname, UUID.randomUUID(), DateProvider.now()));

    ensure(format("Nickname is %s", newNickname), () -> getNickname().equals(newNickname));
    ensure("Generated a UserRegistered", () -> UserEvents.NicknameChanged.class.equals(this.getLastDomainEvent().getClass()));
}
```

- check(String annotation, BooleanSupplier assertion)

The check method is an assertion to reiterate a condition that should be true during the execution of a method body. The first argument is the annotation of the check. The second argument is a Boolean lambda expression used to define a check. If a check is violated, a CheckViolationException is thrown, and the annotation is attached to the exception for debugging purposes.

The following code snippet shows an example of checking the board is not null.

```java
    check("board is not null", () -> board != null);
```

- boolean checkUnsupportedOperation(Runnable runnable)

In DDD, if a reference to a non-root entity is passed outside the aggregate boundary, the receiver holding the reference should refrain from modifying the entity in a way that violates the protected aggregate invariants. One approach to implement this rule is to return a read-only entity. A read-only entity throws an `UnsupportedOperationException` when its command methods are invoked. uContract uses the `checkUnsupportedOperation` method to verify if a command method of a read-only entity follow this rule. The `checkUnsupportedOperation` method accepts a lambda invoking the command method and returns true if an `UnsupportedOperationException` is thrown.

The following code snippet shows an example of checking a read-only board.

```java
private boolean _boardIsReadOnly(Board board) {
    return board instanceof ReadOnlyBoard &&
            board.getBoardMembers().getClass().getName().contains("Unmodifiable") &&
            checkUnsupportedOperation(() -> board.setName("board name")) &&
            checkUnsupportedOperation(() -> board.setProjectId(ProjectId.create())) &&
            checkUnsupportedOperation(() -> board.addMember(BoardId.create(), "userId", BoardRole.Member)) &&
            checkUnsupportedOperation(() -> board.addMember(new BoardMember(BoardId.create(), "userId", BoardRole.Member))) &&
            checkUnsupportedOperation(() -> board.removeMember("userId"));
}
```

- boolean imply(BooleanSupplier a, BooleanSupplier b)

The `imply` method represents implication. It accepts two `BooleanSupplier`.

It is true:

1. If the first argument is false; in this condition the second argument is not evaluated.
2. If both arguments are true.

It is false:

1. If the first argument is true and the second argument is false.

The following code snippet shows an example of checking a shape is a square implies it is not a triangle.

```java
ensure("A shape is a square implies it is not a triangle", 
        () -> imply(() -> isASquare(shape), () -> !isATriangle(shape)));
```

- boolean ifAndOnlyIf(BooleanSupplier a, BooleanSupplier b)

The `ifAndOnlyIf` method represents equivalence. It accepts two `BooleanSupplier`. It is true when both arguments provide the same boolean value; otherwise, it is false.

The following code snippet shows an example of checking a triangle is an equilateral triangle if and only if its three sides are equal.

```java
ensure("A triangle is an equilateral triangle if and only if its three sides are equal",
    () -> ifAndOnlyIf(() -> isEquilateralTriangle(triangle), () -> isThreeSidesEqual(triangle)));
```

- \<T> T old(Supplier\<T> supplier)

The method `old` provides a snapshot of an object. It is used in a method before the method exceution of its body to preserve an object state used in postcondition. The `old` method generates a deep copy of the object provided by the `Supplier` lambda by using JSON serialization. Note that whether the method `old` can successfully make a deep copy is limited by JSON, for more information please reference to [introducing json](https://www.json.org/json-en.html) and [Limitations of Deep Copying Using the JSON.parse and JSON.stringify Method](https://javascript.plainenglish.io/limitations-of-deep-copying-using-the-json-parse-and-json-stringify-method-eef0e337cc61).

The following code snippet shows an example of creating a lane using the `old` method.

```java
public void createLane(String laneId, String name) {
    ...
    var oldLanSize = old(() -> this.getLaneSize());
    apply(new LaneCreated(laneId, name));
    ensure("Lane size increases by 1", () -> this.getLaneSize() == oldLaneSize + 1);
    ...
}
```

### DBC Rules

#### Assertion Monitoring and Evaluation

uContract supports four types of assertions: "pre-conditions", "post-conditions", "class invariants", and "check". They are enabled by default. To disable the four types of assertions, set the environment variables DBC_PRE, DBC_POST, DBC_INV, and DBC_CHECK to off, respectively. To disable all the assertion monitoring, set the environment variable DBC to off.

Writing the conditions as the `BooleanSupplier` lambdas reduces performance overhead when assertions monitoring is disabled. Since lambdas are evaluated inside the method body instead of at the method call, they are evaluated only when necessary. In contrast, if an argument of boolean type is used for the conditions, it is always evaluated at the place of method call regardless of assertions monitoring is enabled or not. The same performance overhead reduction also applies to keyword `old` taking a `Supplier` lambda as the parameter. A clone is performed only if the post-condition check is enabled.

Note that during assertion monitoring, the assertion evaluation rule is implemented by uContract.

#### Documentation support

Contracts can be used as API documentation. uContract supports the collection of contracts during tests execution as a means of generating API documentation via JUnit5 extensions. This feature is disabled by default. To enable it, set the environment variable DBC_DOC to "on" and add `@UContractDocumentation` on test classes. When the test cases are executed the contracts are dumped on console. Users can extend this feature to generate elaborated  API documents.

The following code snippet shows usage of the `@UContractDocumentation`.

```java
@UContractDocumentation
public class TagTest {

    @Test
    public void create_a_valid_tag() {
        TagId tagId = TagId.create();
        BoardId boardId = BoardId.create();
        String tagName = "name";
        String tagColor = "#AABBCC";

        // Trigger contracts in Tag constructor.
        Tag tag = new Tag(boardId, tagId, tagName, tagColor, "userId");

        // Assertion blocks.
        assertEquals(...);
    }
}
```

The following code snippet shows the contracts in Tag constructor.

```java
public Tag(BoardId boardId, TagId tagId, String name, String color, String userId) {
        super();

        requireNotNull("boardId", boardId);
        requireNotNull("name", name);
        requireNotNull("color", color);
        requireNotNull("userId", userId);

        create(boardId, tagId, name, color, userId);

        ensure(format("Tag's id '%s' equals aggregate id '%s'", getTagId(), getId()), () -> getTagId().equals(getId()));
        ensure(format("Tag's board id is '%s'", boardId.id()), () -> boardId.equals(getBoardId()));
        ensure(format("Tag's id is '%s'", tagId.id()), () -> tagId.equals(getTagId()));
        ensure(format("Tag's name is '%s'", name), () -> name.equals(getName()));
        ensure(format("Tag color is '%s'", color), () -> color.equals(getColor()));
        ensure("Tag is not deleted", () -> !isDeleted());
        ensure("A TagCreated event is generated correctly", () -> getLastDomainEvent().equals(new TagEvents.TagCreated(boardId, tagId, name, color, userId, getLastDomainEvent().id(), getLastDomainEvent().occurredOn())));
        ensure("Tag event's aggregate id equals to tag id", () -> getLastDomainEvent().aggregateId().equals(getTagId().id()));
    }
```

The screenshot shows the documentation output of the example above.

![Documentation Output](img/documentation_output.png)

### Usage Example

```Java
public class User extends EsAggregateRoot {
    private UserState state;
    
    // region: aggregate invariants
    public void ensureInvariant() {
        invariant("is not marked as deleted", () -> !isDeleted());
        invariantNotNull("User state", state);
        invariantNotNull("User Id", state.userId());
        invariantNotNull("User name", state.username());
        invariantNotNull("Password", state.password());
        invariantNotNull("Email", state.email());
        invariantNotNull("Nickname", state.nickname());
        invariantNotNull("Role", state.role());
    }
    // endregion
    
    public void changeEmail(String newEmail) {
        // region: preconditions
        requireNotNull("Email", newEmail);
        require("Email is not empty", () -> !newEmail.isEmpty());
        // endregion
        
        var oldState = old(() -> state);
        
        // region: method body
        apply(new EmailChanged(
                state.userId(),
                newEmail,
                EventId.create(),
                Instant.now()));
        // endregion

        // region: postconditions
        ensure(format("Email is %s", newEmail), () -> state.email().equals(newEmail));
        ensureAssignable(state, oldState, ".*email");
        ensure("Generated a UserRegistered", () -> EmailChanged.class.equals(this.getLastDomainEvent().getClass()));
        // endregion
    }
}
```
