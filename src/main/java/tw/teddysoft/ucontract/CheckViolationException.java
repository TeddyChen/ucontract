package tw.teddysoft.ucontract;

/**
 * {@code CheckViolationException} is the subclass of {@code RuntimeException}
 * When the keyword check is violated then {@code CheckViolationException} will
 * be thrown.
 *
 * @author Teddy Chen
 * @author EzKanban Team
 */
public class CheckViolationException extends RuntimeException {

    /**
     * Constructs a new check violation exception.
     */
    public CheckViolationException() {
        super();
    }

    /**
     * Constructs a new check violation exception with the specified detail message.
     *
     * @param message the message to be displayed when the exception is thrown.
     */
    public CheckViolationException(String message) {
        super(message);
    }

    /**
     * Constructs a new check violation exception with the specified detail message and
     * cause.
     *
     * @param message the message to be displayed when the exception is thrown.
     * @param cause   the cause of the exception.
     */
    public CheckViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
