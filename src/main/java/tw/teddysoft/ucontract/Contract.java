package tw.teddysoft.ucontract;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import java.io.IOException;
import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * {@code Contract} is a class for specifying pre-conditions, post-conditions
 * and class invariants.
 *
 * @author Teddy Chen
 * @author EzKanban Team
 * @see PreconditionViolationException
 * @see PostconditionViolationException
 * @see ClassInvariantViolationException
 * @see CheckViolationException
 * @since 0.1
 */
public class Contract {
    /**
     * The constant {@code DBC} is for controlling the evaluation of contracts.
     * The default value of {@code DBC} is {@code true};
     * When environment variable DBC=on, {@code DBC} is {@code true},
     * and contracts will be evaluated.
     * When environment variable DBC=off, {@code DBC} is {@code false},
     * and contracts will not be evaluated.
     */
    public static boolean DBC;

    /**
     * The constant {@code CHECK_PRE} is for controlling the evaluation
     * of pre-conditions.
     * The default value of {@code CHECK_PRE} is {@code true};
     * When {@code CHECK_PRE} is {@code true}, pre-condition will be evaluated.
     * When {@code CHECK_PRE} is {@code false}, pre-condition
     * will not be evaluated.
     */
    public static boolean CHECK_PRE;

    /**
     * The constant {@code CHECK_POST} is for controlling the evaluation
     * of post-conditions.
     * The default value of {@code CHECK_POST} is {@code true};
     * When {@code CHECK_POST} is {@code true}, post-condition
     * will be evaluated.
     * When {@code CHECK_POST} is {@code false}, post-condition
     * will not be evaluated.
     */
    public static boolean CHECK_POST;

    /**
     * The constant {@code CHECK_INV} is for controlling the evaluation
     * of class invariants.
     * The default value of {@code CHECK_INV} is {@code true};
     * When {@code CHECK_INV} is {@code true}, class invariants
     * will be evaluated.
     * When {@code CHECK_INV} is {@code false}, class invariants
     * will not be evaluated.
     */
    public static boolean CHECK_INV;

    /**
     * The constant {@code CHECK_CHECK} is for controlling the evaluation
     * of keyword check.
     * The default value of {@code CHECK_CHECK} is {@code true};
     * When {@code CHECK_CHECK} is {@code true}, keyword check
     * will be evaluated.
     * When {@code CHECK_CHECK} is {@code false}, keyword check
     * will not be evaluated.
     */
    public static boolean CHECK_CHECK;

    /**
     * The constant {@code GENERATE_DOC} is for controlling the documentation
     * generating after execution of contract.
     * The default value of {@code GENERATE_DOC} is {@code false};
     * When {@code GENERATE_DOC} is {@code true}, the document
     * will be generated.
     * When {@code GENERATE_DOC} is {@code false}, the document
     * will not be generated.
     */
    public static boolean GENERATE_DOC;

    /**
     * The constant {@code runtime} is for storing the documentation string.
     */
    public static Map<String, List<String>> runtime = new HashMap<>();

    private static final JsonMapper mapper;

    static {
        Logger logger = Logger.getLogger("uContract");
        DBC = true;
        GENERATE_DOC = false;
        var dbc_env = System.getenv("DBC");
        logger.info("DBC ENV: " + dbc_env);
        if ("off".equals(dbc_env)) {
            DBC = false;
        }

        var doc = System.getenv("DBC_DOC");
        logger.info("DBC_DOC: " + doc);
        if ("on".equals(doc)) {
            GENERATE_DOC = true;
        }

        CHECK_PRE = DBC;
        CHECK_POST = DBC;
        CHECK_INV = DBC;
        CHECK_CHECK = DBC;

        var pre_env = System.getenv("DBC_PRE");
        if ("off".equals(pre_env)){
            logger.info("PRE ENV: " + pre_env);
            CHECK_PRE = false;
        }

        var post_env = System.getenv("DBC_POST");
        if ("off".equals(post_env)){
            logger.info("POST ENV: " + post_env);
            CHECK_POST = false;
        }

        var inv_env = System.getenv("DBC_INV");
        if ("off".equals(inv_env)){
            logger.info("INV ENV: " + inv_env);
            CHECK_INV = false;
        }

        var check_env = System.getenv("DBC_CHECK");
        if ("off".equals(check_env)){
            logger.info("CHECK ENV: " + check_env);
            CHECK_CHECK = false;
        }

        mapper = JsonMapper.builder()
                .addModule(new ParameterNamesModule())
                .addModule(new Jdk8Module())
                .addModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
                .build();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    /**
     * Check a condition at a certain stage.
     *
     * @param annotation the description of the check
     * @param assertion a lambda expression to represent the assertion
     *                  of the pre-condition
     * @throws CheckViolationException if the assertion returns false
     */
    public static void check(String annotation, BooleanSupplier assertion) {
        var msg = format("Check %s", annotation);
        checkImpl(msg, assertion);
    }

    /**
     * Check if the operation is supported or not.
     *
     * @param runnable the function to check if it is unsupported.
     * @return {@code true} if the operation is unsupported.
     */
    public static boolean checkUnsupportedOperation(Runnable runnable) {
        try {
            runnable.run();
            return false;
        } catch (UnsupportedOperationException e) {
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check a pre-condition.
     *
     * @param annotation the description of the pre-condition
     * @param assertion  a lambda expression to represent the assertion
     *                   of the pre-condition
     * @throws PreconditionViolationException
     *          if the {@code assertion} returns {@code false}
     */
    public static void require(String annotation, BooleanSupplier assertion) {
        var msg = format("Require %s", annotation);
        recordContract(msg);
        requireImpl(msg, assertion);
    }

    /**
     * Check a pre-condition of an object cannot be {@code null}.
     *
     * @param annotation the description of the pre-condition
     * @param obj        the object
     * @throws PreconditionViolationException
     *          if the {@code obj} is {@code null}
     */
    public static void requireNotNull(String annotation, Object obj){
        var msg = format("Require [%s] cannot be null", annotation);
        recordContract(msg);
        requireImpl(msg, () -> null != obj);
    }

    /**
     * Check a pre-condition of a string cannot be empty.
     *
     * @param annotation the description of the pre-condition
     * @param str        the string
     * @throws PreconditionViolationException if the {@code str} is empty
     */
    public static void requireNotEmpty(String annotation, String str) {
        var msg = format("Require [%s] cannot be empty", annotation);
        recordContract(msg);
        requireImpl(msg, () -> !str.isEmpty());
    }

    /**
     * Check a post-condition.
     *
     * @param annotation the description of the post-condition
     * @param assertion  a lambda expression to represent the assertion
     *                   of the post-condition
     * @throws PostconditionViolationException
     *          if the {@code assertion} returns {@code false}
     */
    public static void ensure(String annotation, BooleanSupplier assertion) {
        var msg = format("Ensure %s", annotation);
        recordContract(msg);
        ensureImpl(msg, assertion);
    }

    /**
     * Check a post-condition of an object cannot be {@code null}.
     *
     * @param annotation the description of the post-condition
     * @param obj        the object
     * @throws PostconditionViolationException
     *          if the {@code obj} is {@code null}
     */
    public static void ensureNotNull(String annotation, Object obj){
        var msg = format("Ensure [%s] cannot be null", annotation);
        recordContract(msg);
        ensureImpl(msg, () -> null != obj);
    }

    /**
     * Check a post-condition and return {@code result}.
     *
     * @param <T>         the type of {@code result}
     * @param annotation  the description of the post-condition
     * @param result      the returning result
     * @param assertion   a lambda expression that accepts the {@code result}
     *                    to represent the assertion of the post-condition
     * @return the {@code result} if the {@code assertion} is {@code true}
     * @throws PostconditionViolationException
     *          if the {@code assertion} is {@code false}
     */
    public static <T> T ensureResult(String annotation, T result, Function<T, Boolean> assertion) {
        var msg = format("Ensure result [%s]", annotation);
        recordContract(msg);
        return ensureResultImpl(msg, result, assertion);
    }

    /**
     * Check if {@code resultingCollection} is immutable in post-condition.
     *
     * @param <T>                 the type of {@code resultingCollection}
     * @param resultingCollection the returning Collection
     * @return the {@code resultingCollection} if it is immutable
     * @throws PostconditionViolationException
     *          if {@code resultingCollection} is not immutable
     */
    public static <T> T ensureImmutableCollection(T resultingCollection) {
        var msg = format("Ensure resultingCollection is an immutable collection");
        recordContract(msg);
        return ensureResultImpl(msg, resultingCollection, x -> x.getClass().getSimpleName().startsWith("Unmodifiable") || x.getClass().getSimpleName().startsWith("ListN"));
    }

    /**
     * Check the fields except for {@code assignables} are not changed.
     *
     * @param <T> the type of {@code state} and {@code oldState}
     * @param state the object after changing state
     * @param oldState the object before changing state
     * @param assignables regular expressions for name of the fields that can be changed
     * @throws PostconditionViolationException
     *          if any field not assignable is changed
     */
    public static <T> void ensureAssignable(T state, T oldState, String ... assignables) {
        var msg = "";
        recordContract(msg);
        ensureImpl(msg, () -> {
            try {
                assertThat(state)
                        .usingRecursiveComparison()
                        .ignoringFieldsMatchingRegexes(assignables)
                        .isEqualTo(oldState);
            } catch (AssertionError e) {
                throw new PostconditionViolationException(e.getMessage());
            }
            return true;
        });
    }

    /**
     * Check a class invariant.
     *
     * @param annotation  the description of the class invariant
     * @param assertion   a lambda expression to represent the assertion
     *                    of the class invariant
     * @throws ClassInvariantViolationException
     *          if the {@code assertion} is {@code false}
     */
    public static void invariant(String annotation, BooleanSupplier assertion) {
        var msg = format("Ensure Invariant [%s]", annotation);
        recordContract(msg);
        invariantImpl(msg, assertion);
    }

    /**
     * Check a class invariant of an object cannot be {@code null}.
     *
     * @param annotation the description of the class invariant
     * @param obj        the object
     * @throws ClassInvariantViolationException
     *          if the {@code obj} is {@code null}
     */
    public static void invariantNotNull(String annotation, Object obj) {
        var msg = format("Invariant [%s] cannot be null", annotation);
        recordContract(msg);
        invariantImpl(msg, () -> null != obj);
    }

    /**
     * Return the boolean value of the assertion to tell the caller
     * whether to reject the operation and return immediately.
     *
     * @param annotation the description of the rejection
     * @param assertion  a lambda expression to represent the assertion
     *                   for rejection
     * @return {@code true} when the caller have to reject the operation.
     */
    public static boolean reject(String annotation, BooleanSupplier assertion) {
        recordContract(format("Reject %s", annotation));
        return assertion.getAsBoolean();
    }

    /**
     * To represent the semantic of <b>imply</b>.
     * {@code a} implies {@code b} means that if {@code a} is {@code true}
     * then {@code b} is {@code true}.
     *
     * @param a the lambda expression to represent the assertion
     * @param b the lambda expression to represent the assertion
     * @return {@code true} when {@code a} implies {@code b}
     */
    public static boolean imply(BooleanSupplier a, BooleanSupplier b) {
        return !a.getAsBoolean() || b.getAsBoolean();
    }

    /**
     * To represent the semantic of <b>follows from</b>.
     * {@code a} follows from {@code b} means that if {@code b} is {@code true}
     * then {@code a} is {@code true}.
     *
     * @param a the lambda expression to represent the assertion
     * @param b the lambda expression to represent the assertion
     * @return {@code true} when {@code a} follows from {@code b}
     */
    public static boolean followsFrom(BooleanSupplier a, BooleanSupplier b) {
        return a.getAsBoolean() || !b.getAsBoolean();
    }

    /**
     * To represent the semantic of {@code a} <b>if and only if</b> {@code b}.
     * {@code a} if and only if {@code b} means that
     * {@code a} is {@code true} if {@code b} is {@code true}, and
     * {@code b} is {@code true} if {@code a} is {@code true}.
     *
     * @param a the lambda expression to represent the assertion
     * @param b the lambda expression to represent the assertion
     * @return {@code true} when {@code a} if and only if {@code b}
     */
    public static boolean ifAndOnlyIf(BooleanSupplier a, BooleanSupplier b) {
        var aResult = a.getAsBoolean();
        var bResult = b.getAsBoolean();
        return (aResult && bResult) || (!aResult && !bResult);
    }

    /**
     * Denote the value that supplier has which is used in post-conditions.
     *
     * @param <T> the type of object that {@code supplier} provides
     * @param supplier a lambda expression provides an object
     * @return a deep copy of the object provided by {@code supplier}
     */
    public static <T> T old(Supplier<T> supplier) {
        if (!CHECK_POST) {
            return null;
        }
        if (isCalledByAssertion(1)) {
            return null;
        }
        var json = asBytes(supplier.get());
        return (T) readJsonAs(json, supplier.get().getClass());
    }

    private static <T> T ensureResultImpl(String annotation, T returnValue, Function<T, Boolean> assertion) {
        if (!CHECK_POST) return returnValue;
        if (isCalledByAssertion(2)) {
            return returnValue;
        }
        if (assertion.apply(returnValue)) return returnValue;
        throw new PostconditionViolationException(annotation);
    }

    private static void ensureImpl(String annotation, BooleanSupplier assertion) {
        if (!CHECK_POST)
            return;
        if (isCalledByAssertion(2)) {
            return;
        }
        if (!assertion.getAsBoolean()) {
            throw new PostconditionViolationException(annotation);
        }
    }

    private static void invariantImpl(String annotation, BooleanSupplier assertion) {
        if (!CHECK_INV)
            return;
        if (isCalledByAssertion(2)) {
            return;
        }
        if (!assertion.getAsBoolean()) {
            throw new ClassInvariantViolationException(annotation);
        }
    }

    private static void checkImpl(String annotation, BooleanSupplier assertion) {
        if (!CHECK_CHECK)
            return;
        if (isCalledByAssertion(2)) {
            return;
        }
        if (!assertion.getAsBoolean()) {
            throw new CheckViolationException(annotation);
        }
    }

    private static void requireImpl(String annotation, BooleanSupplier assertion) {
        if (!CHECK_PRE)
            return;
        if (isCalledByAssertion(2)) {
            return;
        }
        if (!assertion.getAsBoolean()) {
            throw new PreconditionViolationException(annotation);
        }
    }

    private static void recordContract(String newContract) {
        if (!GENERATE_DOC) return;

        final StackWalker.StackFrame stack = StackWalker.getInstance()
                .walk(s -> s.limit(3).skip(2).collect(Collectors.toList())).get(0);
        String key = stack.getClassName() + "::" + stack.getMethodName();

        if (runtime.containsKey(key)){
            for(var existingContract : runtime.get(key)){
                if (isDuplicatedContract(existingContract, newContract))
                    return;
            }
            runtime.get(key).add(newContract);
        }else{
            runtime.put(key, new ArrayList<>());
            runtime.get(key).add(newContract);
        }
    }


    private static boolean isDuplicatedContract(String str1, String str2){
        return eraseArguments(str1).equals(eraseArguments(str2));
    }


    private static String eraseArguments(String runtimeContract){
        String regular = "'[^\\\\s']*'";
        Pattern pattern = Pattern.compile(regular);
        Matcher matcher = pattern.matcher(runtimeContract);
        List<String> foundTokens = new ArrayList<>();
        while (matcher.find()) {
            foundTokens.add(matcher.group(0));
        }
        for(var each : foundTokens){
            runtimeContract = runtimeContract.replace(each, "");
        }
        return runtimeContract;
    }

    private static boolean isCalledByAssertion(int NumberOfStacksInCurrentAssertion) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        var contractClass = stackTraceElements[1].getClassName();
        final int NUMBER_OF_STACK_OF_THIS_METHOD = 1;
        var totalNumbersContractsOfStacks = Arrays.stream(stackTraceElements).filter(x -> x.getClassName().equals(contractClass)).toList().size();
        if ((totalNumbersContractsOfStacks - NUMBER_OF_STACK_OF_THIS_METHOD) > NumberOfStacksInCurrentAssertion) {
            return true;
        }
        return false;
    }

    private static byte[] asBytes(Object value){
        try {
            return mapper.writeValueAsBytes(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static <A> A readJsonAs(byte[] content, Class<A> clazz) {

        try {
            return mapper.readValue(content, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}