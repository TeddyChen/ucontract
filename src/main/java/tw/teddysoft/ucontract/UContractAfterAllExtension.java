package tw.teddysoft.ucontract;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * {@code UContractAfterAllExtension} is a class for generating documentations.
 *
 * @author Teddy Chen
 * @author EzKanban Team
 * @since 0.10.2
 */
public class UContractAfterAllExtension implements AfterAllCallback {

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        var keys = Contract.runtime.keySet();
        for (var key : keys){
            System.out.println(key);
            for(var eachContract : Contract.runtime.get(key)){
                    System.out.println(eachContract);
            }
            System.out.println();
        }
    }
}
