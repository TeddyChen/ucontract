package tw.teddysoft.ucontract;


/**
 * {@code PostconditionViolationException} is the subclass
 * of {@code RuntimeException}
 * When the post-condition is violated
 * then {@code PostconditionViolationException} will be thrown
 *
 * @author Teddy Chen
 * @author EzKanban Team
 */
public class PostconditionViolationException extends RuntimeException {

    /**
     * Constructs a new post-condition violation exception.
     */
    public PostconditionViolationException() {
        super();
    }

    /**
     * Constructs a new post-condition violation exception with
     * the specified detail message.
     *
     * @param message the message to be displayed when the exception is thrown.
     */
    public PostconditionViolationException(String message) {
        super(message);
    }

    /**
     * Constructs a new post-condition violation exception with
     * the specified detail message and cause.
     *
     * @param message the message to be displayed when the exception is thrown.
     * @param cause   the cause of the exception.
     */
    public PostconditionViolationException(String message, Throwable cause) {
        super(message, cause);
    }

}
