package tw.teddysoft.ucontract;

/**
 * {@code PreconditionViolationException} is the subclass of {@code RuntimeException}
 * When the pre-condition is violated then {@code PreconditionViolationException} will be thrown
 *
 * @author Teddy Chen
 * @author EzKanban Team
 */
public class PreconditionViolationException extends RuntimeException {

    /**
     * Constructs a new pre-condition violation exception.
     */
    public PreconditionViolationException() {
        super();
    }

    /**
     * Constructs a new pre-condition violation exception with the specified detail message.
     *
     * @param message the message to be displayed when the exception is thrown.
     */
    public PreconditionViolationException(String message) {
        super(message);
    }

    /**
     * Constructs a new pre-condition violation exception with the specified detail message and
     * cause.
     *
     * @param message the message to be displayed when the exception is thrown.
     * @param cause   the cause of the exception.
     */
    public PreconditionViolationException(String message, Throwable cause) {
        super(message, cause);
    }

}
