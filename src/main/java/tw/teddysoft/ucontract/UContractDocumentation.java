package tw.teddysoft.ucontract;

import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@code UContractDocumentation} is an annotation to signal the annotated
 * test class should generate documentation for {@code require}, {@code ensure},
 * and {@code invariant}.
 *
 * @author Teddy Chen
 * @author EzKanban Team
 * @see UContractAfterAllExtension
 * @since 0.10.2
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(UContractAfterAllExtension.class)
public @interface UContractDocumentation {
}
