package tw.teddysoft.ucontract;

/**
 * {@code ClassInvariantViolationException} is the subclass of {@code RuntimeException}
 * When the class invariant is violated then {@code ClassInvariantViolationException} will be thrown
 *
 * @author Teddy Chen
 * @author EzKanban Team
 */
public class ClassInvariantViolationException extends RuntimeException {

    /**
     * Constructs a new class invariant violation exception.
     */
    public ClassInvariantViolationException() {
        super();
    }

    /**
     * Constructs a new class invariant violation exception with the specified detail message.
     *
     * @param message the message to be displayed when the exception is thrown.
     */
    public ClassInvariantViolationException(String message) {
        super(message);
    }

    /**
     * Constructs a new class invariant violation exception with the specified detail message and
     * cause.
     *
     * @param message the message to be displayed when the exception is thrown.
     * @param cause   the cause of the exception.
     */
    public ClassInvariantViolationException(String message, Throwable cause) {
        super(message, cause);
    }

}
