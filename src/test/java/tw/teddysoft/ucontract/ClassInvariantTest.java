package tw.teddysoft.ucontract;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.ucontract.Contract.invariant;
import static tw.teddysoft.ucontract.Contract.invariantNotNull;

public class ClassInvariantTest {

    @Test
    public void keyword_invariant_should_not_throw_exception_when_true() {
        assertDoesNotThrow(() -> invariant("true", () -> true));
    }

    @Test
    public void keyword_invariant_should_throw_exception_when_false() {
        var err = assertThrows(ClassInvariantViolationException.class, () -> invariant("true", () -> false));

        assertEquals("Ensure Invariant [true]", err.getMessage());
    }

    @Test
    public void keyword_invariant_not_null_should_not_throw_exception_when_is_not_empty() {
        assertDoesNotThrow(() -> invariantNotNull("object", new Object()));
    }

    @Test
    public void keyword_invariant_not_null_should_throw_exception_when_is_empty() {
        var err = assertThrows(ClassInvariantViolationException.class, () -> invariantNotNull("object", null));

        assertEquals("Invariant [object] cannot be null", err.getMessage());
    }
}
