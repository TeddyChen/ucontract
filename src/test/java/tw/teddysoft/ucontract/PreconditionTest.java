package tw.teddysoft.ucontract;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static tw.teddysoft.ucontract.Contract.*;
import static tw.teddysoft.ucontract.Contract.requireNotEmpty;

public class PreconditionTest {

    @Test
    public void keyword_require_should_not_throw_exception_when_true() {
        assertDoesNotThrow(() -> require("true", () -> true));
    }

    @Test
    public void keyword_require_should_throw_exception_when_false() {
        var err = assertThrows(PreconditionViolationException.class, () -> require("true", () -> false));

        assertEquals("Require true", err.getMessage());
    }

    @Test
    public void keyword_require_not_null_should_not_throw_exception_when_not_null() {
        assertDoesNotThrow(() -> requireNotNull("object", new Object()));
    }

    @Test
    public void keyword_require_not_null_should_throw_exception_when_null() {
        var err = assertThrows(PreconditionViolationException.class, () -> requireNotNull("object", null));

        assertEquals("Require [object] cannot be null", err.getMessage());
    }

    @Test
    public void keyword_require_not_empty_should_not_throw_exception_when_string_is_not_empty() {
        assertDoesNotThrow(() -> requireNotEmpty("string", "str"));
    }

    @Test
    public void keyword_require_not_empty_should_throw_exception_when_string_is_empty() {
        var err = assertThrows(PreconditionViolationException.class, () -> requireNotEmpty("string", ""));

        assertEquals("Require [string] cannot be empty", err.getMessage());
    }

}
