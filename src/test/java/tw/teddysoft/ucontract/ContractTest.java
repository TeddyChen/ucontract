package tw.teddysoft.ucontract;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static tw.teddysoft.ucontract.Contract.*;

public class ContractTest {

    @Test
    public void keyword_check_should_not_throw_exception_when_true() {
        assertDoesNotThrow(() -> check("true", () -> true));
    }

    @Test
    public void keyword_check_should_throw_exception_when_false() {
        var err = assertThrows(CheckViolationException.class, () -> check("true", () -> false));

        assertEquals("Check true", err.getMessage());
    }

    @Test
    public void keyword_reject_execute_correctly() {
        assertTrue(reject("true", () -> true));
        assertFalse(reject("true", () -> false));
    }

    @Test
    public void keyword_old_should_capture_the_current_state(){
        List<String> strings = new ArrayList<>();
        var oldList = old(() -> strings);
        strings.add("1");
        assertEquals(0, oldList.size());
    }

    @Test
    public void keyword_check_unsupported_operation_execute_correctly(){
        assertTrue(checkUnsupportedOperation(() -> {
            throw new UnsupportedOperationException();
        }));
        assertFalse(checkUnsupportedOperation(() -> {
            throw new RuntimeException();
        }));
        assertFalse(checkUnsupportedOperation(() -> {
            new ArrayList<>();
        }));
    }

    @Test
    public void testImply() {
        // 0 0 1
        assertTrue(imply(() -> 1 == 2, () -> 0 > 1));

        // 0 1 1
        assertTrue(imply(() -> 1 == 2, () -> 1 == 1));

        // 1 0 0
        assertFalse(imply(() -> 1 == 1, () -> 1 == 2));

        // 1 1 1
        assertTrue(imply(() -> 1 == 1, () -> 1 != 2));

    }

    @Test
    public void testFollowsFrom() {
        // 0 0 1
        assertTrue(followsFrom(() -> 1 == 2, () -> 0 > 1));

        // 0 1 0
        assertFalse(followsFrom(() -> 1 == 2, () -> 1 == 1));

        // 1 0 1
        assertTrue(followsFrom(() -> 1 == 1, () -> 1 == 2));

        // 1 1 1
        assertTrue(followsFrom(() -> 1 == 1, () -> 1 != 2));

    }

    @Test
    public void testIfAndOnlyIf() {
        // 0 0 1
        assertTrue(ifAndOnlyIf(() -> 1 == 2, () -> 0 > 1));

        // 0 1 0
        assertFalse(ifAndOnlyIf(() -> 1 == 2, () -> 1 == 1));

        // 1 0 0
        assertFalse(ifAndOnlyIf(() -> 1 == 1, () -> 1 == 2));

        // 1 1 1
        assertTrue(ifAndOnlyIf(() -> 1 == 1, () -> 1 != 2));
    }
}
