package tw.teddysoft.ucontract;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static tw.teddysoft.ucontract.Contract.*;
import static tw.teddysoft.ucontract.Contract.ensureAssignable;

public class PostconditionTest {

    @Test
    public void keyword_ensure_should_not_throw_exception_when_true() {
        assertDoesNotThrow(() -> ensure("true", () -> true));
    }

    @Test
    public void keyword_ensure_should_throw_exception_when_false() {
        var err = assertThrows(PostconditionViolationException.class, () -> ensure("true", () -> false));

        assertEquals("Ensure true", err.getMessage());
    }

    @Test
    public void keyword_ensure_not_null_should_not_throw_exception_when_not_null() {
        assertDoesNotThrow(() -> ensureNotNull("object", new Object()));
    }

    @Test
    public void keyword_ensure_not_null_should_throw_exception_when_null() {
        var err = assertThrows(PostconditionViolationException.class, () -> ensureNotNull("object", null));

        assertEquals("Ensure [object] cannot be null", err.getMessage());
    }

    @Test
    public void keyword_ensure_immutable_collection_when_collection_is_immutable() {
        var unmodifiableList = Collections.unmodifiableList(new ArrayList());
        assertSame(unmodifiableList, ensureImmutableCollection(unmodifiableList));
    }

    @Test
    public void keyword_ensure_immutable_collection_should_throw_exception_when_collection_is_not_immutable() {
        var list = new ArrayList();
        var err = assertThrows(PostconditionViolationException.class, () -> ensureImmutableCollection(list));

        assertEquals("Ensure resultingCollection is an immutable collection", err.getMessage());
    }


    @Test
    public void keyword_ensure_result_when_assertion_is_true() {
        var list = new ArrayList<>();
        assertSame(list, ensureResult("list size is zero", list, x -> x.size() == 0));
    }

    @Test
    public void keyword_ensure_result_should_throw_exception_when_assertion_is_false() {
        var list = new ArrayList<>();
        var err = assertThrows(PostconditionViolationException.class, () -> ensureResult("list size is zero", list, x -> x.size() != 0));

        assertEquals("Ensure result [list size is zero]", err.getMessage());
    }

    @Test
    public void keyword_ensure_assignable_with_no_assignables() {
        User user = new User("User id", "Teddy", 20, "teddy@gmail.com");

        var oldUser = old(() -> user);
        assertDoesNotThrow(() -> ensureAssignable(user, oldUser));
    }

    @Test
    public void keyword_ensure_assignable_with_no_assignables_and_change_state() {
        User user = new User("User id", "Teddy", 20, "teddy@gmail.com");

        var oldUser = old(() -> user);
        user.age = 21;
        assertThrows(PostconditionViolationException.class, () -> ensureAssignable(user, oldUser));
    }

    @Test
    public void keyword_ensure_assignable_with_one_field_assignable() {
        User user = new User("User id", "Teddy", 20, "teddy@gmail.com");

        var oldUser = old(() -> user);
        user.age = 21;
        assertDoesNotThrow(() -> ensureAssignable(user, oldUser, "age"));
    }

    @Test
    public void keyword_ensure_assignable_with_one_field_assignable_and_changes_non_assignable_field() {
        User user = new User("User id", "Teddy", 20, "teddy@gmail.com");

        var oldUser = old(() -> user);
        user.age = 21;
        assertThrows(PostconditionViolationException.class, () -> ensureAssignable(user, oldUser, "name"));
    }

    @Test
    public void keyword_ensure_assignable_with_two_fields_assignable() {
        User user = new User("User id", "Teddy", 20, "teddy@gmail.com");

        var oldUser = old(() -> user);
        user.age = 21;
        user.email = "teddy@teddysoft.com";
        assertDoesNotThrow(() -> ensureAssignable(user, oldUser, "age", "email"));
    }

    static class User {
        private String userId;
        private String name;
        private int age;
        private String email;

        public User() {}

        public User(String userId, String name, int age, String email) {
            this.userId = userId;
            this.name = name;
            this.age = age;
            this.email = email;
        }
    }
}
